<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body>
                <h2>My Flowers Collection</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>Name</th>
                        <th>Soil</th>
                        <th>Origin</th>
                        <th>colorOfSteam</th>
                        <th>colorOfLeaf</th>
                        <th>AverageSize</th>
                        <th>Temperature</th>
                        <th>Lightings</th>
                        <th>Watering</th>
                        <th>Multiplying</th>
                    </tr>

                    <xsl:for-each select="Flowers/flower">
                        <tr>
                            <td><xsl:value-of select="name"/></td>
                            <td><xsl:value-of select="soil"/></td>
                            <td><xsl:value-of select="origin"/></td>
                            <td><xsl:value-of select="parameters/colorOfSteam"/></td>
                            <td><xsl:value-of select="parameters/colorOfLeaf"/></td>
                            <td><xsl:value-of select="parameters/averageSize"/></td>
                            <td><xsl:value-of select="growingTips/temperature"/></td>
                            <td><xsl:value-of select="growingTips/lighting"/></td>
                            <td><xsl:value-of select="growingTips/watering"/></td>
                            <td><xsl:value-of select="multiplying"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>