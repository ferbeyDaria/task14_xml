package com.epam.model;

public class Greenhouse {
    private int flowerNo;
    private String name;
    private String soil;
    private String origin;
    private VisualParameters parameters;
    private GrowingTips tips;
    private String multiplying;

    public Greenhouse() {
    }

    public Greenhouse(int flowerNo, String name, String soil, String origin, VisualParameters parameters, GrowingTips tips, String multiplying) {
        this.flowerNo = flowerNo;
        this.name = name;
        this.soil = soil;
        this.origin = origin;
        this.parameters = parameters;
        this.tips = tips;
        this.multiplying = multiplying;
    }

    public int getFlowerNo() {
        return flowerNo;
    }

    public void setFlowerNo(int flowerNo) {
        this.flowerNo = flowerNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSoil() {
        return soil;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public VisualParameters getParameters() {
        return parameters;
    }

    public void setParameters(VisualParameters parameters) {
        this.parameters = parameters;
    }

    public GrowingTips getTips() {
        return tips;
    }

    public void setTips(GrowingTips tips) {
        this.tips = tips;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }

    @Override
    public String toString() {
        return "Greenhouse{" +
                "flowerNo=" + flowerNo +
                ", name='" + name + '\'' +
                ", soil='" + soil + '\'' +
                ", origin='" + origin + '\'' +
                ", parameters=" + parameters +
                ", tips=" + tips +
                ", multiplying='" + multiplying + '\'' +
                '}';
    }
}
