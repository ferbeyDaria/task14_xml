package com.epam.model;

public class VisualParameters {
    private String colorOfSteam;
    private String colorOfLeaf;
    private double averageSize;

    public VisualParameters() {
    }

    public VisualParameters(String colorOfSteam, String colorOfLeaf, double averageSize) {
        this.colorOfSteam = colorOfSteam;
        this.colorOfLeaf = colorOfLeaf;
        this.averageSize = averageSize;
    }

    public String getColorOfSteam() {
        return colorOfSteam;
    }

    public void setColorOfSteam(String colorOfSteam) {
        this.colorOfSteam = colorOfSteam;
    }

    public String getColorOfLeaf() {
        return colorOfLeaf;
    }

    public void setColorOfLeaf(String colorOfLeaf) {
        this.colorOfLeaf = colorOfLeaf;
    }

    public double getAverageSize() {
        return averageSize;
    }

    public void setAverageSize(double averageSize) {
        this.averageSize = averageSize;
    }

    @Override
    public String toString() {
        return "VisualParameters{" +
                "colorOfSteam='" + colorOfSteam + '\'' +
                ", colorOfLeaf='" + colorOfLeaf + '\'' +
                ", averageSize=" + averageSize +
                '}';
    }
}
