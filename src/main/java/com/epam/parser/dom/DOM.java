package com.epam.parser.dom;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

public class DOM {
    public static void main(String[] args) {

        try {
            File inputFile = new File("src/main/resources/xml/greenhouseXML.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
            NodeList nList = doc.getElementsByTagName("flower");
            System.out.println("----------------------------");

            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                System.out.println("\nCurrent Element :" + nNode.getNodeName());

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    System.out.println("Flower number: "
                            + eElement.getAttribute("flowerNo"));
                    System.out.println("Name : "
                            + eElement
                            .getElementsByTagName("name")
                            .item(0)
                            .getTextContent());
                    System.out.println("Soil: "
                            + eElement
                            .getElementsByTagName("soil")
                            .item(0)
                            .getTextContent());
                    System.out.println("Origin : "
                            + eElement
                            .getElementsByTagName("origin")
                            .item(0)
                            .getTextContent());
                    System.out.println("Visul parameters : "
                            + eElement
                            .getElementsByTagName("parameters")
                            .item(0)
                            .getTextContent());
                    System.out.println("Growing tips : "
                            + eElement
                            .getElementsByTagName("growingTips")
                            .item(0)
                            .getTextContent());
                    System.out.println("Multiplying : "
                            + eElement
                            .getElementsByTagName("multiplying")
                            .item(0)
                            .getTextContent());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
